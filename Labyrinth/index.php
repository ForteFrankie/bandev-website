<?php

session_start();

$packageName = "org.bandev.labyrinth";

$id = "22240804";


if($id != "20169836"){
    $category = "DeveloperApplication";
}else{
    $category = "ReferenceApplication";
}

$resp1 = file_get_contents("https://gitlab.com/api/v4/projects/$id");

$project = json_decode($resp1, true);

?>

    <html><head>

<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "SoftwareApplication",
      "name": "<?php echo $project['name'];?>",
      "description": "<?php echo $project['description'];?>",
      "operatingSystem": "ANDROID",
      "publisher":  { "@context" : "https://schema.org",
    "@type" : "Organization",
      "url" : "https://bandev.uk",
      "email": "hello(at)bandev.uk",
      "name": "<?php echo $project['namespace']['name'];?>",
      "logo": "https://upload.wikimedia.org/wikipedia/commons/a/a1/Bandev_400x400.png",
      "slogan": "Open, Free & Secure",
      "description": "BanDev is a collaborative group of developers. We create Android apps that are entirely Free, Libre and Open Source, built on the principles of privacy as a fundamental human right.",
      "sameAs": "https://www.wikidata.org/wiki/Q104591766"
  },
      "installUrl": "https://play.google.com/store/apps/details?id=<?php echo $packageName;?>",
      "applicationCategory": "<?php echo $category?>",
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "5",
        "ratingCount": "6"
      },
      "offers": {
        "@type": "Offer",
        "price": "0",
        "priceCurrency": "GBP"
      }
    }
    </script>

        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&amp;display=swap" rel="stylesheet">
<link href="https://unpkg.com/@primer/css/dist/primer.css" rel="stylesheet">
<link href="../styles/index.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicons/favicon-16x16.png">
    <link rel="manifest" href="../favicons/site.webmanifest">
    <link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#5e17eb">
    <meta name="msapplication-TileColor" content="#5e17eb">
    <meta name="theme-color" content="#5e17eb">
    
<link rel="stylesheet" href="../styles/mob-desk.css">

<title><?php echo $project['name'];?> | BanDev</title>
    </head>
    <body>
        
        
        <div class="container-md clearfix mt-5">

<img class="rounded-logo" src="../images/bandev.png" height="50px" width="auto">

        
                
 <?php if(empty($_SESSION['user'])){
                echo '<a href="../login" class="btn btn-primary mr-2 float-right" type="button">Login</a>';
            }else{
                echo '     
                
                  <details class="dropdown details-reset details-overlay d-inline-block float-right">
    <summary aria-haspopup="true">
        <div class="avatar-parent-child d-inline-flex float-right">
              <img class="avatar" alt="jonrohan" src="'.$_SESSION['user']['avatar_url'].'" width="45" height="45" />
              <img class="avatar avatar-child" src="'.$_SESSION['user']['service_avatar_url'].'" width="20" height="20" />
            </div>
    </summary>

    <ul class="dropdown-menu dropdown-menu-w mr-3">
      <li><a class="dropdown-item pr-3" href="https://bandev.uk/account">Account Center</a></li>
      <li><a class="dropdown-item" href="https://bandev.uk/account/logout">Logout</a></li>
    </ul>
  </details>
                
                
                
              ';
            }?>
                
                
                
              
<div class="pagehead mt-3 mb-5">
  <h3>
    <span class="author" style="font-family: montserrat; font-weight: 700;"><?php echo $project['name'];?></span>
  </h3>
</div>

<div class="Box">
  <div class="blankslate">
    <img src="<?php echo $project['avatar_url'];?>" alt="" class="rounded-logo mb-3">
    <h3 class="mb-1" style="font-family: montserrat; font-weight: 700;"><?php echo $project['name'];?></h3>
    <p><?php echo $project['description'];?></p>
    <a class="btn btn-primary my-3" type="button" href="../library/all">Install Latest</a>
    <p><a class="btn-link" type="button" href="https://play.google.com/store/apps/details?id=<?php echo $packageName?>">Google Play Store</a></p>
  </div>
</div>

<div class="Box mt-5 mb-5">
  <div class="Box-body">
    Service Status: <span class="Label mr-1 Label--green">Ok</span>, Service Maintenance: <span class="Label mr-1 Label--green">Not planned</span> 
    <a class="float-right desk-only" href="BUG">Report a bug</a>
  </div>
  

</div>

  <a class="mob-only pb-5" href="BUG">Report a bug</a>
</div>
    
</body></html>
