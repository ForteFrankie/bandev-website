<?php
session_start()
?>
<html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet">
<link href="https://unpkg.com/@primer/css/dist/primer.css" rel="stylesheet" />
<link href="../styles/index.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicons/favicon-16x16.png">
    <link rel="manifest" href="../favicons/site.webmanifest">
    <link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#5e17eb">
    <meta name="msapplication-TileColor" content="#5e17eb">
    <meta name="theme-color" content="#5e17eb">
    
<link rel="stylesheet" href="../styles/mob-desk.css">

<title>Privacy Policy | BanDev</title>
    </head>
    <body>
        
        
        <div class="container-md clearfix mt-5">

<img class="rounded-logo" src="../images/bandev.png" height="50px" width="auto">

   <?php if(empty($_SESSION['user'])){
                echo '<a href="../login" class="btn btn-primary mr-2 float-right" type="button">Login</a>';
            }else{
                echo '     
                
                  <details class="dropdown details-reset details-overlay d-inline-block float-right">
    <summary aria-haspopup="true">
        <div class="avatar-parent-child d-inline-flex float-right">
              <img class="avatar" alt="jonrohan" src="'.$_SESSION['user']['avatar_url'].'" width="45" height="45" />
              <img class="avatar avatar-child" src="'.$_SESSION['user']['service_avatar_url'].'" width="20" height="20" />
            </div>
    </summary>

    <ul class="dropdown-menu dropdown-menu-w mr-3">
      <li><a class="dropdown-item pr-3" href="https://bandev.uk/account">Account Center</a></li>
      <li><a class="dropdown-item" href="https://bandev.uk/account/logout">Logout</a></li>
    </ul>
  </details>
                
                
                
              ';
            }?>

<div class="pagehead mt-3 mb-5">
  <h3>
    <span class="author" style="font-family: montserrat; font-weight: 700;">Privacy Policy</span>
  </h3>
</div>

<div class="Box p-3">
<p>Your privacy is important to us. It is BanDev's policy to respect your privacy regarding any information we may collect from you across our website, <a href="https://bandev.uk">https://bandev.uk</a>, and other sites we own and operate.</p>
<p>We only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful means, with your knowledge and consent. We also let you know why we’re collecting it and how it will be used.</p>
<p>We only retain collected information for as long as necessary to provide you with your requested service. What data we store, we’ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorised access, disclosure, copying, use or modification.</p>
<p>We don’t share any personally identifying information publicly or with third-parties, except when required to by law.</p>
<p>Our website may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.</p>
<p>You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.</p>
<p>Your continued use of our website will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us.</p>
<p>This policy is effective as of 1 January 2021.</p>

</div>

<div class="Box mt-5 mb-5">
  <div class="Box-body">
    Service Status: <span class="Label mr-1 Label--green">Ok</span>, Service Maintenance: <span class="Label mr-1 Label--green">Not planned</span> 
    <a class="float-right desk-only" href="BUG">Report a bug</a>
  </div>
  

</div>

  <a class="mob-only pb-5" href="BUG">Report a bug</a>
</div>
    </body>
</html>