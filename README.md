# BanDev Website
It's our website.

<h2 id="license">License</h2>
<p><a href="http://www.gnu.org/licenses/gpl-3.0.en.html"><img src="https://www.gnu.org/graphics/gplv3-127x51.png" alt="GNU GPLv3 Image"></a>  </p>
<p>The BanDev Website is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the <a href="https://www.gnu.org/licenses/gpl.html">GNU General Public License</a> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p>