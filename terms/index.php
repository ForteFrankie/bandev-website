<?php
session_start()
?>
<html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap" rel="stylesheet">
<link href="https://unpkg.com/@primer/css/dist/primer.css" rel="stylesheet" />
<link href="../styles/index.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicons/favicon-16x16.png">
    <link rel="manifest" href="../favicons/site.webmanifest">
    <link rel="mask-icon" href="../favicons/safari-pinned-tab.svg" color="#5e17eb">
    <meta name="msapplication-TileColor" content="#5e17eb">
    <meta name="theme-color" content="#5e17eb">
    
<link rel="stylesheet" href="../styles/mob-desk.css">

<title>Terms & Contiditions | BanDev</title>
    </head>
    <body>
        
        
        <div class="container-md clearfix mt-5">

<img class="rounded-logo" src="../images/bandev.png" height="50px" width="auto">

   <?php if(empty($_SESSION['user'])){
                echo '<a href="../login" class="btn btn-primary mr-2 float-right" type="button">Login</a>';
            }else{
                echo '     
                
                  <details class="dropdown details-reset details-overlay d-inline-block float-right">
    <summary aria-haspopup="true">
        <div class="avatar-parent-child d-inline-flex float-right">
              <img class="avatar" alt="jonrohan" src="'.$_SESSION['user']['avatar_url'].'" width="45" height="45" />
              <img class="avatar avatar-child" src="'.$_SESSION['user']['service_avatar_url'].'" width="20" height="20" />
            </div>
    </summary>

    <ul class="dropdown-menu dropdown-menu-w mr-3">
      <li><a class="dropdown-item pr-3" href="https://bandev.uk/account">Account Center</a></li>
      <li><a class="dropdown-item" href="https://bandev.uk/account/logout">Logout</a></li>
    </ul>
  </details>
                
                
                
              ';
            }?>

<div class="pagehead mt-3 mb-5">
  <h3>
    <span class="author" style="font-family: montserrat; font-weight: 700;">Terms & Contiditions</span>
  </h3>
</div>

<div class="Box p-3">
<h3 style="font-family: montserrat; font-weight: 700;">Terms</h3>
<p>By accessing the website at <a href="https://bandev.uk">https://bandev.uk</a>, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trademark law.</p>
<h3 style="font-family: montserrat; font-weight: 700;">Use License</h3>
<ol type="a" class="pl-3 pr-3 mb-2">
   <li>Permission is granted to temporarily download one copy of the materials (information or software) on BanDev's website for personal, non-commercial transitory viewing only. This is the grant of a licence, not a transfer of title, and under this licence you may not:
   <ol type="i" class="pl-3 pr-3">
       <li>modify or copy the materials;</li>
       <li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li>
       <li>attempt to decompile or reverse engineer any software contained on BanDev's website;</li>
       <li>remove any copyright or other proprietary notations from the materials; or</li>
       <li>transfer the materials to another person or "mirror" the materials on any other server.</li>
   </ol>
    </li>
   <li>This licence shall automatically terminate if you violate any of these restrictions and may be terminated by BanDev at any time. Upon terminating your viewing of these materials or upon the termination of this licence, you must destroy any downloaded materials in your possession whether in electronic or printed format.</li>
</ol>
<h3 style="font-family: montserrat; font-weight: 700;">Disclaimer</h3>
<ol type="a" class="pl-3 pr-3 mb-2">
   <li>The materials on BanDev's website are provided on an 'as is' basis. BanDev makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.</li>
   <li>Further, BanDev does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.</li>
</ol>
<h3 style="font-family: montserrat; font-weight: 700;">Limitations</h3>
<p>In no event shall BanDev or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on BanDev's website, even if BanDev or a BanDev authorised representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>
<h3 style="font-family: montserrat; font-weight: 700;">Accuracy of Materials</h3>
<p>The materials appearing on BanDev's website could include technical, typographical, or photographic errors. BanDev does not warrant that any of the materials on its website are accurate, complete or current. BanDev may make changes to the materials contained on its website at any time without notice. However BanDev does not make any commitment to update the materials.</p>
<h3 style="font-family: montserrat; font-weight: 700;">Links</h3>
<p>BanDev has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by BanDev of the site. Use of any such linked website is at the user's own risk.</p>
<h3 style="font-family: montserrat; font-weight: 700;">Modifications</h3>
<p>BanDev may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.</p>
<h3 style="font-family: montserrat; font-weight: 700;">Governing Law</h3>
<p>These terms and conditions are governed by and construed in accordance with the laws of United Kingdom and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.</p>


</div>

<div class="Box mt-5 mb-5">
  <div class="Box-body">
    Service Status: <span class="Label mr-1 Label--green">Ok</span>, Service Maintenance: <span class="Label mr-1 Label--green">Not planned</span> 
    <a class="float-right desk-only" href="BUG">Report a bug</a>
  </div>
  

</div>

  <a class="mob-only pb-5" href="BUG">Report a bug</a>
</div>
    </body>
</html>